from django import forms
from .models import *


class RegisterForm(forms.Form):
    activity_name = forms.CharField(label='Activity', max_length=30)
    date = forms.DateField(label='Date',
                           input_formats=['%Y-%m-%d'],
                           widget=forms.DateInput(attrs={'type': 'date'}))
    time = forms.TimeField(label="Time",
                           input_formats=['%H:%M'],
                           widget=forms.TimeInput(attrs={'type': 'time'}))
    location = forms.CharField(label='Location')
    category = forms.CharField(label='Category', max_length=20)


class StatusForm(forms.Form):
    error_messages = {'required': 'Tolong isi input ini!', }

    status_message = forms.CharField(label='Status:', max_length=300, widget=forms.Textarea(
        attrs={'name': 'status-message',
               'placeholder': "What's your status?",
               'class': 'text-field',
               'id': 'content-text',
               }))


class SubscribeForm(forms.ModelForm):
    class Meta:
        model = SubscribeData
        fields = '__all__'

    name = forms.CharField(label='Full Name', max_length=100, widget=forms.TextInput(
        attrs={'id': 'name',
               'class': 'form-control',
               'placeholder': 'Your full name here',
               }))
    email = forms.EmailField(label='Email', max_length=50, widget=forms.TextInput(
        attrs={'id': 'email',
               'class': 'form-control',
               'placeholder': 'Your email here',
               }))
    password = forms.CharField(label='Password', max_length=25, widget=forms.PasswordInput(
        attrs={'id': 'passowrd',
               'class': 'form-control',
               'placeholder': 'Your passowrd here',
               }))
