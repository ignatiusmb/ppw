from django.urls import path

from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('professor', professor, name='professor'),
    path('register', register, name='register'),
    path('register/activity', activity, name='activity'),
]
