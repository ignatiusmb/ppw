from django.db import models

# Create your models here.


class Activity(models.Model):
    activity_name = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    location = models.CharField(max_length=50)
    category = models.CharField(max_length=20)

    def __str__(self):
        return self.activity_name


class SubscribeData(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=50, unique=True)
    password = models.CharField(max_length=50)

    def __str__(self):
        return self.email
