import time
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve

from .views import home as homepage
from .models import Activity
from .forms import RegisterForm, StatusForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class HomePageUnitTest(TestCase):
    # def test_url_homepage_exist(self):
    #     response = Client().get('/')
    #     self.assertEqual(response.status_code, 200)

    def test_homepage_using_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

    # def test_model_can_add_new_status(self):
    #     status_message = Activity.objects.create(
    #         status_message='Halo, saya lagi belajar PPW')

    #     counting_all_status_message = Activity.objects.all().count()
    #     self.assertEqual(counting_all_status_message, 1)

    # def test_if_form_is_blank(self):
    #     form = RegisterForm(data={'status_message': ''})
    #     self.assertFalse(form.is_valid())
    #     self.assertEqual(
    #         form.errors['status_message'][0],
    #         'This field is required.',
    #     )

    # def test_self_func(self):
    #     Activity.objects.create(status_message='HELLO')
    #     status = Activity.objects.get(id=1)
    #     self.assertEqual(str(status), status.status_message)

    # def test_text_max_char(self):
    #     status = Activity.objects.create(
    #         status_message="TEST TEST TEST TEST TEST TEST")
    #     self.assertLessEqual(len(str(status)), 300)


# class HomePageFunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome(
#             './chromedriver', chrome_options=chrome_options)
#         super(HomePageFunctionalTest, self).setUp()

#     def test_input_and_result_in_same_page(self):
#         selenium = self.selenium
#         # Opening the tester link
#         selenium.get(self.live_server_url)
#         # Find form element
#         status_message = selenium.find_element_by_name('status_message')
#         submit = selenium.find_element_by_id('submit')
#         time.sleep(3)

#         # Fill form with data
#         status_message.send_keys('Coba Coba')

#         # Submitting the form
#         submit.send_keys(Keys.RETURN)

#         self.assertIn("Status!", selenium.title)
#         self.assertIn("Coba Coba", selenium.page_source)

#     def test_navigation_exist(self):
#         selenium = self.selenium
#         selenium.get('https://imb-ppw-lab.herokuapp.com')

#         nav_tag = selenium.find_element_by_tag_name('nav').text
#         self.assertIn('This is a navigation bar', nav_tag)

#     def test_heading_exist(self):
#         selenium = self.selenium
#         selenium.get('https://imb-ppw-lab.herokuapp.com')

#         head_tag = selenium.find_element_by_tag_name('h1').text
#         self.assertIn('Hello! This is a heading', head_tag)

#     def test_navigation_display_flex(self):
#         selenium = self.selenium
#         selenium.get('https://imb-ppw-lab.herokuapp.com')

#         nav_bar = selenium.find_element_by_tag_name(
#             'nav').value_of_css_property('display')
#         self.assertIn(nav_bar, 'flex')

#     def test_profile_color_white(self):
#         selenium = self.selenium
#         selenium.get('https://imb-ppw-lab.herokuapp.com')

#         profile_section = selenium.find_element_by_class_name(
#             'profile').value_of_css_property('color')
#         self.assertIn(profile_section, 'white')

#     def test_form_result_display_flex(self):
#         selenium = self.selenium
#         selenium.get('https://imb-ppw-lab.herokuapp.com')

#         form_result = selenium.find_element_by_class_name(
#             'form-result').value_of_css_property('display')
#         self.assertIn(form_result, 'flex')

#     def tearDown(self):
#         time.sleep(3)
#         self.selenium.quit()
#         super(HomePageFunctionalTest, self).tearDown()
