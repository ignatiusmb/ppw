from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from .forms import *
from .models import *


# Create your views here.

def home(request):
    return render(request, 'home.html')


def professor(request):
    return render(request, 'professor.html')


def register(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Activity.objects.create(**data)
            return HttpResponseRedirect('/register/activity')
    else:
        form = RegisterForm()
    context = {'form': form}
    return render(request, 'register.html', context)


def activity(request):
    if request.method == 'POST':
        Activity.objects.all().delete()
        return HttpResponseRedirect('/register/activity')
    list_activity = list(Activity.objects.all())
    list_activity.reverse()
    context = {'activity': list_activity}
    return render(request, 'activity.html', context)


def subscribe(request):
    return render(request, 'subscribe.html')


def validate_email(request):
    if request.method == 'POST':
        email = request.POST['email']
        check_email = SubscribeData.objects.filter(email=email)
        if check_email.exist():
            return JsonResponse({'is_exists': True})
        return JsonResponse({'is_exists': False})
